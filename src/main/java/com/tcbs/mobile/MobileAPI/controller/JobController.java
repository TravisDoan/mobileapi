//package com.tcbs.mobile.MobileAPI.controller;
//
//import com.tcbs.mobile.MobileAPI.message.ResponseData;
//import com.tcbs.mobile.MobileAPI.service.base.JobService;
//import org.json.JSONException;
//import org.json.JSONObject;
//import org.slf4j.Logger;
//import org.slf4j.LoggerFactory;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.web.bind.annotation.CrossOrigin;
//import org.springframework.web.bind.annotation.RequestMapping;
//import org.springframework.web.bind.annotation.RestController;
//
//import java.io.IOException;
//import java.util.Map;
//
//@RestController
//@RequestMapping("job")
//public class JobController {
//    private final Logger logger = LoggerFactory.getLogger(this.getClass());
//
//    @Autowired
//    private JobService jobService;
//
//    @RequestMapping("/test")
//    public String testService() throws JSONException {
//        return "job";
//    }
//
//    @CrossOrigin(origins = "*")
//    @RequestMapping("/truncateBondInfo")
//    public ResponseData<String> truncateBondInfo() throws JSONException, IOException {
//        JSONObject responseTruncateBondInfo = jobService.jobDeleteOldDataSalesKitBondInfoTCInvest();
//        if (responseTruncateBondInfo.getString("msg").equals("0")) {
//            return new ResponseData<String>(0,"SUCCESS","Truncate Success!");
//        }
//        return new ResponseData<String>(1,"FAIL","Truncate Fail!");
//    }
//
//    @CrossOrigin(origins = "*")
//    @RequestMapping("/updateAvailableForSaleBondInfo")
//    public ResponseData<Map<String, Object>> updateAvailableForSaleDaily() throws JSONException, IOException {
//        Map<String, Object> responseUpdateAvalableForSaleDaily = jobService.jobUpdateSalesKitBondInfoFromTCBondOld();
//        return new ResponseData<Map<String, Object>>(0,"SUCCESS", responseUpdateAvalableForSaleDaily);
//    }
//
//    @CrossOrigin(origins = "*")
//    @RequestMapping("/updateMaturityHoldRateBondInfo")
//    public ResponseData<Map<String, Object>> updateMaturityHoldRateDaily() throws JSONException, IOException {
//        Map<String, Object> responseUpdateMaturityHoldRateDaily = jobService.jobUpdateMaturityHoldRateForSalesKitBondInfoTCInvest();
//        return new ResponseData<Map<String, Object>>(0,"SUCCESS", responseUpdateMaturityHoldRateDaily);
//    }
//
//    @CrossOrigin(origins = "*")
//    @RequestMapping("/generateAdvisorBondInfo")
//    public ResponseData<Map<String, Object>> getBondInfoForIAdvisor() throws JSONException, IOException {
//        Map<String, Object> responseGetBondInfoForIAdvisor = jobService.jobWrappingSalesKitBondInfoForIAdvisorAndSaveToFile();
//        if (new JSONObject(responseGetBondInfoForIAdvisor).isNull("bondInfo")) {
//            return new ResponseData<Map<String, Object>>(1,"FAIL", responseGetBondInfoForIAdvisor);
//        }
//        return new ResponseData<Map<String, Object>>(0,"SUCCESS", responseGetBondInfoForIAdvisor);
//    }
//
//
//
//}
