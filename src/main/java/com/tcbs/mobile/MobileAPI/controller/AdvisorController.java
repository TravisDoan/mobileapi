package com.tcbs.mobile.MobileAPI.controller;


import com.tcbs.mobile.MobileAPI.message.ResponseData;
import com.tcbs.mobile.MobileAPI.service.base.AdvisorService;
import com.tcbs.mobile.MobileAPI.utils.AdvisorUtils;
import io.reactivex.Observable;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;
import java.util.stream.Collectors;

import io.reactivex.*;


@RestController
@RequestMapping("/api")
public class AdvisorController {
    private final Logger logger = LoggerFactory.getLogger(this.getClass());

    @Autowired
    private AdvisorService advisorService;


    @RequestMapping("/test")
    public String testService() throws JSONException {
//        System.out.print("Test Success!");
        return "api";
    }

//    @CrossOrigin(origins = "*")
//    @RequestMapping("/getBondInfo")
//    public Map<String, Object> getBondInfo() throws JSONException, IOException {
//        Map<String, Object> parentMap = new HashMap<String, Object>();
//        Map<String, Object> map = new HashMap<String, Object>();
//        JSONObject jsonData = advisorService.getBondInfo();
//        JSONArray jArray = jsonData.getJSONArray("entry");
//        List<Map<String, Object>> listMap = new ArrayList<Map<String, Object>>();
//        for (int i = 0; i < jArray.length(); i++) {
//            Map<String, Object> subMap = new HashMap<String, Object>();
//            JSONObject keyObject = jArray.getJSONObject(i);
//            Iterator iterSubKey = keyObject.keys();
//            while (iterSubKey.hasNext()) {
//                String subKey = (String) iterSubKey.next();
//                Object objValue = keyObject.get(subKey);
//                subMap = AdvisorUtils.putObjectDataToMap(objValue, keyObject, subKey, subMap);
//            }
//            listMap.add(subMap);
//        }
//        map.put("entry", listMap);
//        parentMap.put("entries", map);
//        return parentMap;
//    }

    @CrossOrigin(origins = "*")
    @RequestMapping("/getSalesKitProduct")
    public Map<String, Object> getSalesKitProduct() throws JSONException, IOException {
        Map<String, Object> parentMap = new HashMap<String, Object>();
        Map<String, Object> map = new HashMap<String, Object>();
        JSONObject jsonData = advisorService.getSalesKitProduct();
        JSONArray jArray = jsonData.getJSONArray("entry");
        List<Map<String, Object>> listMap = new ArrayList<Map<String, Object>>();
        for (int i = 0; i < jArray.length(); i++) {
            Map<String, Object> subMap = new HashMap<String, Object>();
            JSONObject keyObject = jArray.getJSONObject(i);
            Iterator iterSubKey = keyObject.keys();
            while (iterSubKey.hasNext()) {
                String subKey = (String) iterSubKey.next();
                Object objValue = keyObject.get(subKey);
                subMap = AdvisorUtils.putObjectDataToMap(objValue, keyObject, subKey, subMap);
            }
            listMap.add(subMap);
        }
        map.put("entry", listMap);
        parentMap.put("entries", map);
        return parentMap;
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(method = RequestMethod.POST, value = "/product")
    public ResponseData<Map<String, Object>> postPricing(@RequestParam(value = "productcode") String productCode,
                                                         @RequestParam(value = "buydate", required = false) String buyDate,
                                                         @RequestParam(value = "quantity") String quantity,
                                                         @RequestParam(value = "_type") String _type,
                                                         @RequestParam(value = "pitfee", required = false) String pitFee,
                                                         @RequestParam(value = "purpose") String purpose,
                                                         @RequestParam(value = "buydatecustom", required = false) String buyDateCustom,
                                                         @RequestParam(value = "buyvolumecustom", required = false) String buyVolumeCustom,
                                                         @RequestParam(value = "buypricecustom", required = false) String buyPriceCustom,
                                                         @RequestParam(value = "sellPriceCustom", required = false) String sellPriceCustom,
                                                         @RequestParam(value = "selldate", required = false) String sellDate,
                                                         @RequestParam(value = "issuercall", required = false) String issuercall,
                                                         @RequestParam(value = "ratecustom", required = false) String rateCustom,
                                                         @RequestParam(value = "rate", required = false) String rate,
                                                         @RequestParam(value = "orderid", required = false) String orderId,
                                                         @RequestParam(value = "customerType", required = false) String customerType,
                                                         @RequestParam(value = "customQTT", required = false) String customQTT) throws JSONException, IOException {
//        System.out.print(buyDate + "----" + productCode);
        Map<String, Object> map = new HashMap<String, Object>();
        if (_type.equals("1")) {
            JSONObject jsonData = advisorService.postBuyPricingEngine(productCode, buyDate, quantity, pitFee, purpose,
                    _type, rateCustom, rate, customerType, customQTT);
            Iterator iterKey = jsonData.keys();
            while (iterKey.hasNext()) {
                String key = (String) iterKey.next();
                if (jsonData.has("errorMessage")) {
                    Object obj = jsonData.get(key);
                    map = AdvisorUtils.putObjectDataToMap(obj, jsonData, key, map);
                    return new ResponseData<Map<String, Object>>(1, "ERROR", map);
                } else {
                    Object objData = jsonData.get(key);
                    if (objData instanceof JSONArray) {
                        JSONArray jArray = jsonData.getJSONArray(key);
                        if (jArray.length() > 0) {
                            Object subObj = jArray.get(0);
                            map = AdvisorUtils.putJSONArrayDataToMap(subObj, jArray, key, map);
                        } else {
                            List<Map<String, Object>> listMap = new ArrayList<>();
                            map.put(key, listMap);
                        }

                    } else {
                        Object obj = jsonData.get(key);
                        map = AdvisorUtils.putObjectDataToMap(obj, jsonData, key, map);
                    }
                }
            }
        } else {
            JSONObject jsonData = advisorService.postSellPricingEngine(productCode, sellDate, quantity, pitFee, purpose,
                    _type, buyDateCustom, buyPriceCustom, buyVolumeCustom, sellPriceCustom, issuercall, orderId);
            Iterator iterKey = jsonData.keys();
            while (iterKey.hasNext()) {
                String key = (String) iterKey.next();
                if (jsonData.has("errorMessage")) {
                    Object obj = jsonData.get(key);
                    map = AdvisorUtils.putObjectDataToMap(obj, jsonData, key, map);
                    return new ResponseData<Map<String, Object>>(1, "ERROR", map);
                } else {
                    Object objData = jsonData.get(key);
                    if (objData instanceof JSONArray) {
                        JSONArray jArray = jsonData.getJSONArray(key);
                        if (jArray.length() > 0) {
                            Object subObj = jArray.get(0);
                            map = AdvisorUtils.putJSONArrayDataToMap(subObj, jArray, key, map);
                        } else {
                            List<Map<String, Object>> listMap = new ArrayList<>();
                            map.put(key, listMap);
                        }

                    } else {
                        Object obj = jsonData.get(key);
                        map = AdvisorUtils.putObjectDataToMap(obj, jsonData, key, map);
                    }
                }

            }
        }
        return new ResponseData<Map<String, Object>>(0, "SUCCESS", map);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping("/getAdvisorBondInfo")
    public ResponseData<List<Map<String, Object>>> getAdvisorBondInfo() throws JSONException, IOException {
        JSONObject jsonData = advisorService.getAdvisorBondInfo();
        JSONArray jsonArrayReadFromFile = jsonData.getJSONArray("bondInfo");
        List<Map<String, Object>> listMap = new ArrayList<Map<String, Object>>();
        for (int i = 0; i < jsonArrayReadFromFile.length(); i++) {
            Map<String, Object> map = new HashMap<String, Object>();
            JSONObject keyObject = jsonArrayReadFromFile.getJSONObject(i);
            Iterator iterSubKey = keyObject.keys();
            while (iterSubKey.hasNext()) {
                String subKey = (String) iterSubKey.next();
                Object objValue = keyObject.get(subKey);
                if (objValue instanceof JSONArray) {
                    JSONArray jArray = keyObject.getJSONArray(subKey);
                    if (jArray.length() > 0) {
                        Object subObj = jArray.get(0);
                        map = AdvisorUtils.putJSONArrayDataToMap(subObj, jArray, subKey, map);
                    } else {
                        List<Map<String, Object>> listSubMap = new ArrayList<>();
                        map.put(subKey, listSubMap);
                    }
                } else {
                    Object obj = keyObject.get(subKey);
                    map = AdvisorUtils.putObjectDataToMap(obj, keyObject, subKey, map);
                }
//                subMap = AdvisorUtils.putObjectDataToMap(objValue, keyObject, subKey, subMap);
            }
            listMap.add(map);
        }
        return new ResponseData<List<Map<String, Object>>>(0, "SUCCESS", listMap);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping("/getAdvisorProduct")
    public ResponseData<List<Map<String, Object>>> getAdvisorProduct() throws JSONException, IOException {
        JSONObject jsonData = advisorService.getAdvisorProduct();
        JSONArray jsonArray = jsonData.getJSONArray("bondInfo");
        List<Map<String, Object>> listMap = new ArrayList<Map<String, Object>>();
        for (int i = 0; i < jsonArray.length(); i++) {
            Map<String, Object> map = new HashMap<String, Object>();
            JSONObject keyObject = jsonArray.getJSONObject(i);
            Iterator iterSubKey = keyObject.keys();
            while (iterSubKey.hasNext()) {
                String subKey = (String) iterSubKey.next();
                Object objValue = keyObject.get(subKey);
                if (objValue instanceof JSONArray) {
                    JSONArray jArray = keyObject.getJSONArray(subKey);
                    if (jArray.length() > 0) {
                        Object subObj = jArray.get(0);
                        map = AdvisorUtils.putJSONArrayDataToMap(subObj, jArray, subKey, map);
                    } else {
                        List<Map<String, Object>> listSubMap = new ArrayList<>();
                        map.put(subKey, listSubMap);
                    }
                } else {
                    Object obj = keyObject.get(subKey);
                    map = AdvisorUtils.putObjectDataToMap(obj, keyObject, subKey, map);
                }
            }
            listMap.add(map);
        }
        return new ResponseData<List<Map<String, Object>>>(0, "SUCCESS", listMap);
    }

    @CrossOrigin(origins = "*")
    @RequestMapping(method = RequestMethod.POST, value = "/getSellBeforeExpired")
    public ResponseData<List<Map<String, Object>>> getSellBeforeExpired(@RequestParam(value = "arrayFutureDate", required = false) String arrayFutureDate,
                                                                        @RequestParam(value = "productcode") String productCode,
                                                                        @RequestParam(value = "buydate", required = false) String buyDate,
                                                                        @RequestParam(value = "quantity") String quantity,
                                                                        @RequestParam(value = "pitfee", required = false) String pitFee,
                                                                        @RequestParam(value = "purpose") String purpose,
                                                                        @RequestParam(value = "_type") String _type,
                                                                        @RequestParam(value = "ratecustom", required = false) String rateCustom,
                                                                        @RequestParam(value = "rate", required = false) String rate,
                                                                        @RequestParam(value = "customerType", required = false) String customerType,
                                                                        @RequestParam(value = "buyprice") String buyPrice,
                                                                        @RequestParam(value = "monthsleft", required = false) String monthsLeft,
                                                                        @RequestParam(value = "sellfor") String sellFor) throws JSONException, InterruptedException, ExecutionException {

        String[] strArray = String.join(",", arrayFutureDate).split(",");
        List<String> listArrayFutureDate = Arrays.asList(strArray);
        Map<String, CompletableFuture<Map<String, Object>>> mapTempPricing = new HashMap<>();
        List<CompletableFuture<Map<String, Object>>> arrayListCompletableFuture = listArrayFutureDate.stream().
                map(futureDate -> advisorService.postSellBeforeExpired(listArrayFutureDate, productCode, buyDate, quantity, pitFee, purpose, _type, rateCustom, rate, customerType, buyPrice, monthsLeft, futureDate, sellFor)).
                collect(Collectors.toList());
        CompletableFuture<Void> allFutures = CompletableFuture.allOf(
                arrayListCompletableFuture.toArray(new CompletableFuture[arrayListCompletableFuture.size()])
        );
        CompletableFuture<List<Map<String, Object>>> allPageContentsFuture = allFutures.thenApply(v -> {
            return arrayListCompletableFuture.stream()
                    .map(pageContentFuture -> pageContentFuture.join())
                    .collect(Collectors.toList());
        });
        return new ResponseData<List<Map<String, Object>>>(0, "SUCCESS", allPageContentsFuture.get());
    }

    @CrossOrigin(origins = "*")
    @RequestMapping("/getYieldCurveInfo")
    public ResponseData<List<Map<String, Object>>> getYieldCurveInfo() throws JSONException, IOException {
        JSONObject jsonData = advisorService.getYieldCurveInfo();
        JSONArray jsonArray = jsonData.getJSONArray("data");
        List<Map<String, Object>> listMap = new ArrayList<Map<String, Object>>();
        for (int i = 0; i < jsonArray.length(); i++) {
            Map<String, Object> map = new HashMap<String, Object>();
            JSONObject keyObject = jsonArray.getJSONObject(i);
            Iterator iterSubKey = keyObject.keys();
            while (iterSubKey.hasNext()) {
                String subKey = (String) iterSubKey.next();
                Object objValue = keyObject.get(subKey);
                if (objValue instanceof JSONArray) {
                    JSONArray jArray = keyObject.getJSONArray(subKey);
                    if (jArray.length() > 0) {
                        Object subObj = jArray.get(0);
                        map = AdvisorUtils.putJSONArrayDataToMap(subObj, jArray, subKey, map);
                    } else {
                        List<Map<String, Object>> listSubMap = new ArrayList<>();
                        map.put(subKey, listSubMap);
                    }
                } else {
                    Object obj = keyObject.get(subKey);
                    map = AdvisorUtils.putObjectDataToMap(obj, keyObject, subKey, map);
                }
            }
            listMap.add(map);
        }
        return new ResponseData<List<Map<String, Object>>>(0, "SUCCESS", listMap);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/users")
    public Observable<String> getUsers() {
        return Observable.just("Hello world");
    }
}
