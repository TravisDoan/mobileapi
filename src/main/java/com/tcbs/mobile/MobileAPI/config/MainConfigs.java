package com.tcbs.mobile.MobileAPI.config;

import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Import;
import org.springframework.context.annotation.PropertySource;

@Configuration
@EnableAutoConfiguration
@ComponentScan(basePackages = {"com.tcbs.mobile.MobileAPI"})
@PropertySource(value = {"classpath:configs.properties"})
//@Import({JobSchedule.class})
public class MainConfigs {
}
