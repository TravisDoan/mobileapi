package com.tcbs.mobile.MobileAPI.config;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.builder.SpringApplicationBuilder;
import org.springframework.boot.web.support.SpringBootServletInitializer;

@SpringBootApplication
public class MobileApiApplication extends SpringBootServletInitializer {

	private final Logger logger = LoggerFactory.getLogger(this.getClass());

	@Override
    protected SpringApplicationBuilder configure(SpringApplicationBuilder application) {
		setRegisterErrorPageFilter(false);
	    logger.info("Start mobile service ...");
	    return application.sources(MainConfigs.class);
    }

	public static void main(String[] args)  throws  Exception{
		SpringApplication.run(MainConfigs.class, args);
	}
}
