//package com.tcbs.mobile.MobileAPI.config;
//
//
//
//import com.tcbs.mobile.MobileAPI.job.AutowiringSpringJobFactoryBean;
//import com.tcbs.mobile.MobileAPI.job.AdvisorJob;
//import org.quartz.Trigger;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.beans.factory.config.PropertiesFactoryBean;
//import org.springframework.context.ApplicationContext;
//import org.springframework.context.annotation.Bean;
//import org.springframework.context.annotation.Configuration;
//import org.springframework.context.annotation.PropertySource;
//import org.springframework.core.io.ClassPathResource;
//import org.springframework.scheduling.quartz.CronTriggerFactoryBean;
//import org.springframework.scheduling.quartz.JobDetailFactoryBean;
//import org.springframework.scheduling.quartz.SchedulerFactoryBean;
//
//import java.io.IOException;
//import java.util.ArrayList;
//import java.util.List;
//import java.util.Properties;
//
//@Configuration
//@PropertySource(value = {"classpath:configs.properties"})
//public class JobSchedule {
//    private final static String GROUP_ADVISOR_JOBS = "GroupAdvisorJobs";
//
//    @Autowired
//    private ApplicationContext applicationContext;
//
//    @Value("${crontab.advisor.access.token}")
//    private String advisorCrontab;
//
//    @Bean
//    public Properties quartzProperties() throws IOException {
//        PropertiesFactoryBean propertiesFactoryBean = new PropertiesFactoryBean();
//        propertiesFactoryBean.setLocation(new ClassPathResource("/quartz.properties"));
//        propertiesFactoryBean.afterPropertiesSet();
//        return propertiesFactoryBean.getObject();
//    }
//
//    //======>>Job configs
//    @Bean
//    public JobDetailFactoryBean jobCheckTokenSalesforce() {
//        JobDetailFactoryBean jobFactoryBean = new JobDetailFactoryBean();
//        jobFactoryBean.setJobClass(AdvisorJob.class);
//        jobFactoryBean.setName("GetListAdvisorProduct");
//        jobFactoryBean.setGroup(GROUP_ADVISOR_JOBS);
//        return jobFactoryBean;
//    }
//
//    @Bean
//    public CronTriggerFactoryBean cronGetTokenSalesforce() {
//        CronTriggerFactoryBean crFactory = new CronTriggerFactoryBean();
//        crFactory.setJobDetail(jobCheckTokenSalesforce().getObject());
//        crFactory.setStartDelay(2000);
//        crFactory.setCronExpression(advisorCrontab);
//        crFactory.setName("triggerAdvisorToken");
//        return crFactory;
//    }
//
//    //======>> End CronTriggerConfigs
//
//    //======>> SchedulerFactoryBean
//    @Bean
//    public SchedulerFactoryBean schedulerFactoryBean(){
//        SchedulerFactoryBean scheduler = new SchedulerFactoryBean();
//        AutowiringSpringJobFactoryBean jobFactory = new AutowiringSpringJobFactoryBean();
//        jobFactory.setApplicationContext(applicationContext);
//        scheduler.setJobFactory(jobFactory);
//
//        List<Trigger> listTrigger = new ArrayList<Trigger>();
//        listTrigger.add(cronGetTokenSalesforce().getObject());
//        scheduler.setTriggers(listTrigger.toArray(new Trigger[listTrigger.size()]));
//        return scheduler;
//    }
//
//}
