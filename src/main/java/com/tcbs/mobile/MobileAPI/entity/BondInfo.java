package com.tcbs.mobile.MobileAPI.entity;

public class BondInfo {
    int productId;
    String productCode;
    int bondId;
    String bondCode;
    int categoryId;
    String category;
    double par;
    String issueDate;
    String maturityDate;
    String noteType;
    String noteInterest;
    String notePayInterest;
    String issuerName;
    String issuerInfoUrl;
    int availableForSale;
    double maturityHoldRate;
    String isCallable;

    public BondInfo(int productId, String productCode, int bondId, String bondCode, int categoryId, String category,  double par, String issueDate, String maturityDate, String noteType, String noteInterest, String notePayInterest, String issuerName, String issuerInfoUrl, int availableForSale,  double maturityHoldRate, String isCallable) {
        super();
        this.productId = productId;
        this.productCode = productCode;
        this.bondId = bondId;
        this.bondCode = bondCode;
        this.categoryId = categoryId;
        this.category = category;
        this.par = par;
        this.issueDate = issueDate;
        this.maturityDate = maturityDate;
        this.noteType = noteType;
        this.noteInterest = noteInterest;
        this.notePayInterest = notePayInterest;
        this.issuerName = issuerName;
        this.issuerInfoUrl = issuerInfoUrl;
        this.availableForSale = availableForSale;
        this.maturityHoldRate =maturityHoldRate;
        this.isCallable = isCallable;
    }

    public  int getProductId() {
        return productId;
    }

    public void setProductId( int productId) {
        this.productId = productId;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public  int getBondId() {
        return bondId;
    }

    public void setBondId( int bondId) {
        this.bondId = bondId;
    }

    public String getBondCode() {
        return bondCode;
    }

    public void setBondCode(String bondCode) {
        this.bondCode = bondCode;
    }

    public int getCategoryId() {
        return categoryId;
    }

    public void setCategoryId( int categoryId) {
        this.categoryId = categoryId;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public double getPar() {
        return par;
    }

    public void setPar( double par) {
        this.par = par;
    }

    public String getIssueDate() {
        return issueDate;
    }

    public void setIssueDate(String issueDate) {
        this.issueDate = issueDate;
    }

    public String getMaturityDate() {
        return maturityDate;
    }

    public void setMaturityDate(String maturityDate) {
        this.maturityDate = maturityDate;
    }

    public String getNoteType() {
        return noteType;
    }

    public void setNoteType(String noteType) {
        this.noteType = noteType;
    }

    public String getNoteInterest() {
        return noteInterest;
    }

    public void setNoteInterest(String noteInterest) {
        this.noteInterest = noteInterest;
    }

    public String getNotePayInterest() {
        return notePayInterest;
    }

    public void setNotePayInterest(String notePayInterest) {
        this.notePayInterest = notePayInterest;
    }

    public String getIssuerName() {
        return issuerName;
    }

    public void setIssuerName(String issuerName) {
        this.issuerName = issuerName;
    }

    public String getIssuerInfoUrl() {
        return issuerInfoUrl;
    }

    public void setIssuerInfoUrl(String issuerInfoUrl) {
        this.issuerInfoUrl = issuerInfoUrl;
    }

    public int getAvailableForSale() {
        return availableForSale;
    }

    public void setAvailableForSale( int availableForSale) {
        this.availableForSale = availableForSale;
    }

    public double getMaturityHoldRate() {
        return maturityHoldRate;
    }

    public void setMaturityHoldRate( double maturityHoldRate) {
        this.maturityHoldRate = maturityHoldRate;
    }

    public String getIsCallable() {
        return isCallable;
    }

    public void setIsCallable(String isCallable) {
        this.isCallable = isCallable;
    }


}