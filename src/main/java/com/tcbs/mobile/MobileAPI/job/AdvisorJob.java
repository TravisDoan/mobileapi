package com.tcbs.mobile.MobileAPI.job;


import com.tcbs.mobile.MobileAPI.service.base.AdvisorService;
import com.tcbs.mobile.MobileAPI.utils.AdvisorUtils;
import org.json.JSONArray;
import org.json.JSONObject;
import org.quartz.JobExecutionContext;
import org.quartz.JobExecutionException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.quartz.QuartzJobBean;

import java.io.IOException;
import java.util.*;

public class AdvisorJob extends QuartzJobBean {

    @Autowired
    private AdvisorService advisorService;

    @Override
    protected void executeInternal(JobExecutionContext args) throws JobExecutionException {
        JSONObject jsonData = null;
        try {
            jsonData = advisorService.getAdvisorProduct();
            JSONArray jsonArray = jsonData.getJSONArray("bondInfo");
            List<Map<String, Object>> listMap = new ArrayList<Map<String, Object>>();
            for (int i = 0; i < jsonArray.length(); i++) {
                Map<String, Object> map = new HashMap<String, Object>();
                JSONObject keyObject = jsonArray.getJSONObject(i);
                Iterator iterSubKey = keyObject.keys();
                while (iterSubKey.hasNext()) {
                    String subKey = (String) iterSubKey.next();
                    Object objValue = keyObject.get(subKey);
                    if (objValue instanceof JSONArray) {
                        JSONArray jArray = keyObject.getJSONArray(subKey);
                        if (jArray.length() > 0) {
                            Object subObj = jArray.get(0);
                            map = AdvisorUtils.putJSONArrayDataToMap(subObj, jArray, subKey, map);
                        } else {
                            List<Map<String, Object>> listSubMap = new ArrayList<>();
                            map.put(subKey, listSubMap);
                        }
                    } else {
                        Object obj = keyObject.get(subKey);
                        map = AdvisorUtils.putObjectDataToMap(obj, keyObject, subKey, map);
                    }
                }
                listMap.add(map);
                System.out.print(listMap);
            }
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
