//package com.tcbs.mobile.MobileAPI.service.base;
//
//import org.json.JSONException;
//import org.json.JSONObject;
//
//import java.io.IOException;
//import java.util.Map;
//
//public interface JobService {
//    Map<String, Object> jobUpdateSalesKitBondInfoFromTCBondOld() throws JSONException, IOException;
//    JSONObject jobDeleteOldDataSalesKitBondInfoTCInvest() throws JSONException, IOException;
//    Map<String, Object> jobWrappingSalesKitBondInfoForIAdvisorAndSaveToFile() throws JSONException, IOException;
//    Map<String, Object> jobUpdateMaturityHoldRateForSalesKitBondInfoTCInvest() throws JSONException, IOException;
//}
