package com.tcbs.mobile.MobileAPI.service.base;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.ArrayList;

public interface PricingService {
    ArrayList<String> getListRecurringDate(String productCode, String startDate, String endDate);

    JSONObject postBuyPricingEngine(JSONObject obj) throws JSONException, IOException;
}
