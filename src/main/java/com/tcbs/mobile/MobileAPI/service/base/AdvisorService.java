package com.tcbs.mobile.MobileAPI.service.base;

import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.scheduling.annotation.Async;

import java.io.IOException;
import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.concurrent.CompletableFuture;
import java.util.concurrent.ExecutionException;

//import rx.Observable;
public interface AdvisorService {
    JSONObject postBuyPricingEngine(String productCode,
                                    String buyDate,
                                    String quantity,
                                    String pitFee,
                                    String purpose,
                                    String _type,
                                    String rateCustom,
                                    String rate,
                                    String customerType,
                                    String customQTT) throws JSONException, IOException;

    JSONObject postSellPricingEngine(String productCode,
                                     String sellDate,
                                     String quantity,
                                     String pitFee,
                                     String purpose,
                                     String _type,
                                     String buyDateCustom,
                                     String buyPriceCustom,
                                     String buyVolumeCustom,
                                     String sellPriceCustom,
                                     String issuerCall,
                                     String orderId) throws JSONException, IOException;

    JSONObject postMultiplePricing(String productCode,
                                   String calculateDate,
                                   String quantity,
                                   String pitFee,
                                   String purpose,
                                   String _type,
                                   String rateCustom,
                                   String rate,
                                   String customerType,
                                   String buyDateCustom,
                                   String buyPriceCustom,
                                   String buyVolumeCustom,
                                   String issuerCall,
                                   String orderId,
                                   String sellFor) throws JSONException, IOException;

//    JSONObject getBondInfo() throws JSONException, IOException;

    JSONObject getSalesKitProduct() throws JSONException, IOException;

    JSONObject getAdvisorBondInfo() throws JSONException, IOException;

    JSONObject getAdvisorProduct() throws JSONException, IOException;

    JSONObject getYieldCurveInfo() throws JSONException, IOException;

//    JSONObject postSellBeforeExpired(List<String> arrayFutureDate, String productCode, String buyDate, String quantity, String pitFee, String purpose, String _type, String rateCustom, String buyPrice, String monthsLeft) throws JSONException, InterruptedException, ExecutionException;

    @Async
    CompletableFuture<JSONObject> postBuyPricingAsync(String productCode, String buyDate, String quantity, String pitFee, String purpose, String _type, String rateCustom, String rate, String customerType, String customQTT);

    @Async
    CompletableFuture<JSONObject> postSellPricingAsync(String productCode, String sellDate, String quantity, String pitFee, String purpose, String _type, String buyDateCustom, String buyPriceCustom, String buyVolumeCustom, String sellPriceCustom, String issuerCall, String orderId);

    @Async
    CompletableFuture<Map<String, Object>> postSellBeforeExpired(List<String> arrayFutureDate, String productCode, String buyDate, String quantity, String pitFee, String purpose, String _type, String rateCustom, String rate, String customerType, String total, String monthsLeft, String calculateDate, String sellFor);

//    Observable<String> getRxString();
}
