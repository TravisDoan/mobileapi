package com.tcbs.mobile.MobileAPI.service.impl;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.tcbs.mobile.MobileAPI.message.ResponseData;
import com.tcbs.mobile.MobileAPI.service.base.AdvisorService;
import com.tcbs.mobile.MobileAPI.utils.AdvisorUtils;
import com.tcbs.mobile.MobileAPI.utils.GeneralUtils;
import com.tcbs.mobile.MobileAPI.utils.JsonMapConverter;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.lang.reflect.Array;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

@Service
public class AdvisorServiceImpl implements AdvisorService {

    // URL SERVICES
    @Value("${bond.info.file}")
    private String urlSaveFile;

    @Value("${url.esb.pricing.service}")
    private String urlPricingService;

//    @Value("${url.dss.invest.service}")
//    private String urlDSSInvestService;

    @Value("${url.dss.invest2.service}")
    private String urlDSSInvest2Service;

    @Value("${url.dss.pricing.engine.service}")
    private String urlDSSPricingEngineService;

    @Value("${url.dss.starwar.service}")
    private String urlDSSStarwarService;

    @Value("${context.dss.get.invest.bondinfo}")
    private String contextDSSGetBondInfo;

    @Value("${context.dss.get.invest.bondhaspricing}")
    private String contextDSSGetSalesKitProduct;

    @Value("${context.dss.get.bond.getIAdvisorProduct}")
    private String contextDSSGetAdvisorProduct;

    @Value("${context.dss.get.bond.getYieldCurveInfo}")
    private String contextDSSGetYieldCurveInfo;

    // POST SERVICES

    @Override
    public JSONObject postBuyPricingEngine(String productCode, String buyDate, String quantity, String pitFee,
                                           String purpose, String _type, String rateCustom, String rate, String customerType, String customQTT) throws JSONException, IOException {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/x-www-form-urlencoded");
        headers.put("Accept", "application/json");
        Map<String, Object> params = new HashMap<>();
        params.put("productcode", productCode);
        params.put("buydate", buyDate);
        params.put("quantity", quantity);
        params.put("pitfee", pitFee);
        params.put("purpose", purpose);
        params.put("_type", _type);
//        System.out.print(productCode);
        if (rateCustom != null) {
            System.out.println(rateCustom);
            params.put("ratecustom", rateCustom);
        }

        if (rate != null) {
            params.put("rate", rate);
        }

        if (customerType != null) {
            params.put("customerType", customerType);
        }

        if (customQTT != null) {
            params.put("customQTT", customQTT);
        }


        try {

            HttpResponse<JsonNode> jsonResponse = Unirest.post(urlPricingService).headers(headers).fields(params)
                    .asJson();

            JSONObject responseObject = jsonResponse.getBody().getObject();
            if (responseObject.getInt("status") == 0) {
                headers.clear();
                params.clear();
                return responseObject.getJSONObject("data");
            } else {
                Map<String, String> jsonObjError = new HashMap<>();
                jsonObjError.put("errorMessage", responseObject.getString("data"));
                headers.clear();
                params.clear();
                return new JSONObject(jsonObjError);
            }
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public JSONObject postSellPricingEngine(String productCode, String sellDate, String quantity, String pitFee,
                                            String purpose, String _type, String buyDateCustom, String buyPriceCustom, String buyVolumeCustom,
                                            String sellPriceCustom, String issuerCall, String orderId) throws JSONException, IOException {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/x-www-form-urlencoded");
        headers.put("Accept", "application/json");
        Map<String, Object> params = new HashMap<>();
        params.put("productcode", productCode);
        params.put("selldate", sellDate);
        params.put("quantity", quantity);
        params.put("pitfee", pitFee);
        params.put("purpose", purpose);
        params.put("_type", _type);
        params.put("buydatecustom", buyDateCustom);
        params.put("buypricecustom", buyPriceCustom);
        params.put("buyvolumecustom", buyVolumeCustom);


        if (orderId != null) {
            params.put("orderid", orderId);
        }
        if (sellPriceCustom != null) {
            params.put("sellPriceCustom", sellPriceCustom);
        }
        if (issuerCall != null) {
            params.put("issuercall", issuerCall);
        }
        try {

//            System.out.println("TAT TOAN" + params);
            HttpResponse<JsonNode> jsonResponse = Unirest.post(urlPricingService).headers(headers).fields(params)
                    .asJson();

            JSONObject responseObject = jsonResponse.getBody().getObject();
            if (responseObject.getInt("status") == 0) {
//                System.out.print("[Sell Pricing" + sellDate + responseObject.getJSONObject("data"));
                headers.clear();
                params.clear();
                return responseObject.getJSONObject("data");
            } else {
                Map<String, String> jsonObjError = new HashMap<>();
                jsonObjError.put("errorMessage", responseObject.getString("data"));
                headers.clear();
                params.clear();
                return new JSONObject(jsonObjError);
            }
        } catch (UnirestException e) {

            e.printStackTrace();
        }
        return null;
    }

    // GET SERVICES



    @Override
    public JSONObject getSalesKitProduct() throws JSONException, IOException {
        String url = urlPricingService + contextDSSGetSalesKitProduct;
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/x-www-form-urlencoded");
        headers.put("Accept", "application/json");
        try {

            HttpResponse<JsonNode> jsonResponse = Unirest.get(url).headers(headers).asJson();

            JSONObject responseObject = jsonResponse.getBody().getObject();
            if (responseObject.has("entries")) {
                headers.clear();
                return responseObject.getJSONObject("entries");
            } else {
                Map<String, String> jsonObjError = new HashMap<>();
                jsonObjError.put("errorMessage", responseObject.getString("data"));
                headers.clear();
                return new JSONObject(jsonObjError);
            }
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public JSONObject getAdvisorBondInfo() throws JSONException {
        try {
            JSONObject jsonReadFromFile = new JSONObject(GeneralUtils.readJSONFromFile());
            return jsonReadFromFile;
        } catch (Exception e) {
            e.printStackTrace();
            Map<String, Object> jsonObjError = new HashMap<>();
            jsonObjError.put("errorMessage", e);
            return new JSONObject(jsonObjError);
        }
    }

    @Override
    public JSONObject getAdvisorProduct() throws JSONException, IOException {
        String url = urlDSSInvest2Service + contextDSSGetAdvisorProduct;
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/x-www-form-urlencoded");
        headers.put("Accept", "application/json");
        try {
            HttpResponse<JsonNode> jsonResponse = Unirest.get(url).headers(headers).asJson();
            JSONObject responseObject = jsonResponse.getBody().getObject();
            if (responseObject.has("entries")) {
                JSONArray advisorJsonArrayResp = responseObject.getJSONObject("entries").getJSONArray("entry");
                List<JSONObject> advisorArray = new ArrayList<>();
                List<String> categoryBondArray = new ArrayList<>();
                List<JSONObject> activeBondInfoArray = new ArrayList<>();
                List<JSONObject> dataBondInfoArray = new ArrayList<>();
                for (int i = 0; i < advisorJsonArrayResp.length(); i++) {
                    if (!advisorJsonArrayResp.getJSONObject(i).isNull("maturityHoldRate")) {
                        advisorArray.add(advisorJsonArrayResp.getJSONObject(i));
                    }
                    String categoryBondStr = advisorJsonArrayResp.getJSONObject(i).getString("category");
                    if (categoryBondArray.indexOf(categoryBondStr) == -1) {
                        categoryBondArray.add(categoryBondStr);
                    }
                }

                for (String categoryAdvisorArrayItem : categoryBondArray) {
                    Map<String, Object> categoryJsonObject = new HashMap<>();
                    List<JSONObject> categoryJsonArray = new ArrayList<>();
                    advisorArray.sort((o1, o2) -> {
                        try {
                            return Integer.parseInt(o2.getString("availableForSale").replace(".0", "")) - Integer.parseInt(o1.getString("availableForSale").replace(".0", ""));
                        } catch (JSONException e) {
                            e.printStackTrace();
                        }
                        return 0;
                    });

                    for (JSONObject advisorArrayItem : advisorArray) {

                        if (advisorArrayItem.getString("category").equals(categoryAdvisorArrayItem)) {
                            categoryJsonObject.put("categoryID", advisorArrayItem.getString("categoryID").replace(".", "").replace("0", ""));
                            categoryJsonObject.put("categoryName", advisorArrayItem.getString("category"));
                            Map<String, Object> childCategoryJsonObject = new HashMap<>();
                            childCategoryJsonObject.put("productCODE", advisorArrayItem.getString("productCode"));
                            childCategoryJsonObject.put("noteInterest", advisorArrayItem.getString("noteInterest"));
                            childCategoryJsonObject.put("expiredDate", GeneralUtils.getCurrenDate(advisorArrayItem.getString("maturityDate"), GeneralUtils.DateType.NORMAL));
                            childCategoryJsonObject.put("expiredDateIOS", GeneralUtils.getCurrenDate(advisorArrayItem.getString("maturityDate"), GeneralUtils.DateType.IOS));
                            childCategoryJsonObject.put("createdDate", GeneralUtils.getCurrenDate(advisorArrayItem.getString("issueDate"), GeneralUtils.DateType.NORMAL));
                            childCategoryJsonObject.put("createdDateIOS", GeneralUtils.getCurrenDate(advisorArrayItem.getString("issueDate"), GeneralUtils.DateType.IOS));
//                                    childCategoryJsonObject.put("productId", advisorArrayItem.getString("productID"));
                            childCategoryJsonObject.put("notePayInterest", advisorArrayItem.getString("notePayInterest"));
                            childCategoryJsonObject.put("availableForSale", (double) Math.round(Double.parseDouble(advisorArrayItem.getString("availableForSale")) * Double.parseDouble(advisorArrayItem.getString("par")) / 1000000000 * 10) / 10);
                            childCategoryJsonObject.put("maturityHoldRate", advisorArrayItem.getString("maturityHoldRate"));
                            childCategoryJsonObject.put("bondCode", advisorArrayItem.getString("bondCode"));
                            childCategoryJsonObject.put("par", advisorArrayItem.getString("par"));
                            if (!advisorArrayItem.isNull("issuerInfoUrl")) {
                                childCategoryJsonObject.put("issuerInfoUrl", advisorArrayItem.getString("issuerInfoUrl"));
                            } else {
                                childCategoryJsonObject.put("issuerInfoUrl", "");
                            }
                            childCategoryJsonObject.put("isCallable", advisorArrayItem.isNull("isCallable") ? "" : advisorArrayItem.getString("isCallable"));
                            childCategoryJsonObject.put("noteType", advisorArrayItem.getString("noteType"));
                            childCategoryJsonObject.put("issuerName", advisorArrayItem.getString("issuerName"));

                            if (Double.parseDouble(advisorArrayItem.getString("maturityHoldRate")) != 0.0) {
                                categoryJsonArray.add(new JSONObject(childCategoryJsonObject));
                            }

                        }
                    }
                    if (categoryJsonArray.size() > 0) {
                        categoryJsonObject.put("child", categoryJsonArray);
                        dataBondInfoArray.add(new JSONObject(categoryJsonObject));
                    }
                }
                Map<String, Object> dataBondInfoMap = new HashMap<>();
                dataBondInfoMap.put("bondInfo", dataBondInfoArray);
                return new JSONObject(dataBondInfoMap);

            } else {
                Map<String, String> jsonObjError = new HashMap<>();
                jsonObjError.put("errorMessage", responseObject.getString("data"));
                headers.clear();
                return new JSONObject(jsonObjError);
            }
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    @Async
    public CompletableFuture<Map<String, Object>> postSellBeforeExpired(List<String> arrayFutureDate,
                                                                        String productCode, String buyDate, String quantity, String pitFee, String purpose, String _type,
                                                                        String rateCustom, String rate, String customerType, String total, String monthsLeft, String calculateDate, String sellFor) {
        return CompletableFuture.supplyAsync(new Supplier<Map<String, Object>>() {
            @Override
            public Map<String, Object> get() {
                try {
                    Map<String, Object> multiDateResponse = new HashMap<>();
                    int index = arrayFutureDate.indexOf(calculateDate);
                    // JSONObject jsonBuyObj = new JSONObject();
                    // JSONObject jsonSellObj = new JSONObject();
                    if (sellFor.equals("1")) {
                        // jsonBuyObj = postBuyPricingEngine(productCode, calculateDate, quantity,
                        // pitFee, purpose,
                        // _type, rateCustom);
                        // jsonSellObj = postSellPricingEngine(productCode, calculateDate, quantity,
                        // pitFee, purpose, "2",
                        // buyDate, buyPrice, quantity, jsonBuyObj.getString("total"), null, null);
//                        CompletableFuture<CompletableFuture<JSONObject>> cfJsonSellObj = postBuyPricingAsync(
//                                productCode, calculateDate, quantity, pitFee, purpose, _type, rateCustom)
//                                .thenApply(jsonBuyObj -> {
//                                    try {
//                                        System.out
//                                                .println(calculateDate + "----------------------" + jsonBuyObj);
//
//                                        return postSellPricingAsync(productCode, calculateDate, quantity,
//                                                pitFee, purpose, "2", buyDate, buyPrice, quantity,
//                                                jsonBuyObj.getString("total"), null, null);
//                                    } catch (JSONException e) {
//                                        // TODO Auto-generated catch block
//                                        e.printStackTrace();
//                                    }
//                                    return null;
//                                });

                        JSONObject jsonSellObj = postMultiplePricing(productCode, calculateDate, quantity, pitFee, purpose, _type, rateCustom, rate, customerType, buyDate, total, quantity, null, null, sellFor);
//                        System.out.println(calculateDate + "----------------------" + jsonSellObj);
                        Map<String, Object> jsonMapSell = JsonMapConverter.toMap(jsonSellObj);
                        Iterator iterKey = jsonSellObj.keys();
                        while (iterKey.hasNext()) {
                            String key = (String) iterKey.next();
                            if (jsonSellObj.has("errorMessage")) {
                                Object obj = jsonSellObj.get(key);
                                multiDateResponse = AdvisorUtils.putObjectDataToMap(obj, jsonSellObj, key,
                                        multiDateResponse);
                                return multiDateResponse;
                            } else {
                                Object objData = jsonSellObj.get(key);
                                if (objData instanceof JSONArray) {
                                    JSONArray jArray = jsonSellObj.getJSONArray(key);

                                    if (jArray.length() > 0) {
                                        Object subObj = jArray.get(0);
                                        multiDateResponse = AdvisorUtils.putJSONArrayDataToMap(subObj, jArray, key,
                                                multiDateResponse);

                                        if (key.equals("couponreceivedArr")) {
                                            multiDateResponse.put("investRateBeforeExpire",
                                                    jsonMapSell.get("investmentRate"));
                                            multiDateResponse.put("reInvestRateBeforeExpire",
                                                    jsonMapSell.get("investmentRateWithReinvest"));

                                        }
                                    } else {
                                        if (key.equals("couponreceivedArr")) {
                                            multiDateResponse.put("investRateBeforeExpire",
                                                    jsonMapSell.get("investmentRate"));
                                            multiDateResponse.put("reInvestRateBeforeExpire",
                                                    jsonMapSell.get("investmentRate"));
                                        }
                                        List<Map<String, Object>> listMap = new ArrayList<>();
                                        multiDateResponse.put(key, listMap);
                                        listMap.clear();
                                        jsonMapSell.clear();
                                    }

                                } else {
                                    Object obj = jsonSellObj.get(key);
                                    multiDateResponse = AdvisorUtils.putObjectDataToMap(obj, jsonSellObj, key,
                                            multiDateResponse);
                                }
                            }
                        }

                        multiDateResponse.put("rootIndex", index + 1);
                        multiDateResponse.put("globalIndex", Double.parseDouble(monthsLeft) - (index + 1));
                        return multiDateResponse;
                    } else if (sellFor.equals("2")) {

//                        CompletableFuture<CompletableFuture<JSONObject>> cfJsonSellObj = postBuyPricingAsync(
//                                productCode, calculateDate, quantity, pitFee, purpose, _type, rateCustom)
//                                .thenApply(jsonBuyObj -> {
//                                    try {
//                                        return postSellPricingAsync(productCode, calculateDate, quantity,
//                                                pitFee, purpose, "2", buyDate, buyPrice, quantity,
//                                                jsonBuyObj.getString("buyPriceTCBS"), null, null);
//                                    } catch (JSONException e) {
//                                        // TODO Auto-generated catch block
//                                        e.printStackTrace();
//                                    }
//                                    return null;
//                                });
//                        JSONObject jsonSellObj = cfJsonSellObj.get().get();
                        JSONObject jsonSellObj = postMultiplePricing(productCode, calculateDate, quantity, pitFee, purpose, _type, rateCustom, rate, customerType, buyDate, total, quantity, null, null, sellFor);
                        Map<String, Object> jsonMapSell = JsonMapConverter.toMap(jsonSellObj);
                        Iterator iterKey = jsonSellObj.keys();
                        while (iterKey.hasNext()) {
                            String key = (String) iterKey.next();
                            if (jsonSellObj.has("errorMessage")) {
                                Object obj = jsonSellObj.get(key);
                                multiDateResponse = AdvisorUtils.putObjectDataToMap(obj, jsonSellObj, key,
                                        multiDateResponse);
                                return multiDateResponse;
                            } else {
                                Object objData = jsonSellObj.get(key);
                                if (objData instanceof JSONArray) {
                                    JSONArray jArray = jsonSellObj.getJSONArray(key);
                                    if (jArray.length() > 0) {
                                        Object subObj = jArray.get(0);
                                        multiDateResponse = AdvisorUtils.putJSONArrayDataToMap(subObj, jArray, key,
                                                multiDateResponse);
                                        if (key.equals("couponreceivedArr")) {
                                            multiDateResponse.put("investRateBeforeExpire",
                                                    jsonMapSell.get("investmentRate"));
                                            multiDateResponse.put("reInvestRateBeforeExpire",
                                                    jsonMapSell.get("investmentRateWithReinvest"));

                                        }
                                    } else {
                                        if (key.equals("couponreceivedArr")) {
                                            multiDateResponse.put("investRateBeforeExpire",
                                                    jsonMapSell.get("investmentRate"));
                                            multiDateResponse.put("reInvestRateBeforeExpire",
                                                    jsonMapSell.get("investmentRate"));
                                        }
                                        List<Map<String, Object>> listMap = new ArrayList<>();
                                        listMap.clear();
                                        jsonMapSell.clear();
                                        multiDateResponse.put(key, listMap);
                                    }

                                } else {
                                    Object obj = jsonSellObj.get(key);
                                    multiDateResponse = AdvisorUtils.putObjectDataToMap(obj, jsonSellObj, key,
                                            multiDateResponse);
                                }
                            }
                        }
                        multiDateResponse.put("rootIndex", index + 1);
                        multiDateResponse.put("globalIndex", Double.parseDouble(monthsLeft) - (index + 1));
                        return multiDateResponse;
                    } else {

                        JSONObject jsonSellObj = postSellPricingEngine(productCode, calculateDate, quantity, pitFee,
                                purpose, "2", buyDate, total, quantity, null, null, null);
                        Map<String, Object> jsonMapSell = JsonMapConverter.toMap(jsonSellObj);
                        Iterator iterKey = jsonSellObj.keys();
                        while (iterKey.hasNext()) {
                            String key = (String) iterKey.next();
                            if (jsonSellObj.has("errorMessage")) {
                                Object obj = jsonSellObj.get(key);
                                multiDateResponse = AdvisorUtils.putObjectDataToMap(obj, jsonSellObj, key,
                                        multiDateResponse);
                                return multiDateResponse;
                            } else {
                                Object objData = jsonSellObj.get(key);
                                if (objData instanceof JSONArray) {
                                    JSONArray jArray = jsonSellObj.getJSONArray(key);
                                    if (jArray.length() > 0) {
                                        Object subObj = jArray.get(0);
                                        multiDateResponse = AdvisorUtils.putJSONArrayDataToMap(subObj, jArray, key,
                                                multiDateResponse);
//                                        System.out.println(
//                                                "Length" + jsonSellObj.getJSONArray("couponreceivedArr").length());
                                        if (jsonSellObj.getJSONArray("couponreceivedArr").length() > 0) {
                                            multiDateResponse.put("investRateBeforeExpire",
                                                    jsonMapSell.get("investmentRate"));
                                            multiDateResponse.put("reInvestRateBeforeExpire",
                                                    jsonMapSell.get("investmentRateWithReinvest"));

                                        } else {
                                            multiDateResponse.put("investRateBeforeExpire",
                                                    jsonMapSell.get("investmentRate"));
                                            multiDateResponse.put("reInvestRateBeforeExpire",
                                                    jsonMapSell.get("investmentRate"));
                                        }
                                    } else {

                                        List<Map<String, Object>> listMap = new ArrayList<>();
                                        multiDateResponse.put(key, listMap);
                                        listMap.clear();
                                        jsonMapSell.clear();
                                    }

                                } else {
                                    Object obj = jsonSellObj.get(key);
                                    multiDateResponse = AdvisorUtils.putObjectDataToMap(obj, jsonSellObj, key,
                                            multiDateResponse);
                                }
                            }
                        }
                        multiDateResponse.put("rootIndex", index + 1);
                        return multiDateResponse;
                    }

                } catch (Throwable e) {
                    e.printStackTrace();
                    return null;
                }
            }
        });
    }

    @Override
    @Async
    public CompletableFuture<JSONObject> postBuyPricingAsync(String productCode, String buyDate, String quantity,
                                                             String pitFee, String purpose, String _type, String rateCustom, String rate, String customerType, String customQTT) {

        return CompletableFuture.supplyAsync(() -> {
            JSONObject jsonBuyObj = null;
            try {
                jsonBuyObj = postBuyPricingEngine(productCode, buyDate, quantity, pitFee, purpose, _type, rateCustom, rate, customerType, customQTT);
            } catch (JSONException | IOException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            }
            return jsonBuyObj;
        });

    }

    @Override
    @Async
    public CompletableFuture<JSONObject> postSellPricingAsync(String productCode, String sellDate, String quantity,
                                                              String pitFee, String purpose, String _type, String buyDateCustom, String buyPriceCustom,
                                                              String buyVolumeCustom, String sellPriceCustom, String issuerCall, String orderId) {

        return CompletableFuture.supplyAsync(() -> {
            JSONObject jsonSellObj = null;
            try {
                jsonSellObj = postSellPricingEngine(productCode, sellDate, quantity, pitFee, purpose, _type,
                        buyDateCustom, buyPriceCustom, buyVolumeCustom, sellPriceCustom, issuerCall, orderId);
            } catch (JSONException e) {
                // TODO Auto-generated catch block
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            }
            return jsonSellObj;
        });

    }

    @Override
    public JSONObject postMultiplePricing(String productCode, String calculateDate, String quantity, String pitFee, String purpose,
                                          String _type, String rateCustom, String rate, String customerType, String buyDateCustom,
                                          String buyPriceCustom, String buyVolumeCustom, String issuerCall, String orderId, String sellFor) throws JSONException, IOException {
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/x-www-form-urlencoded");
        headers.put("Accept", "application/json");
        Map<String, Object> buyParams = new HashMap<>();
        buyParams.put("productcode", productCode);
        buyParams.put("buydate", calculateDate);
        buyParams.put("quantity", quantity);
        buyParams.put("pitfee", pitFee);
        buyParams.put("purpose", purpose);
        buyParams.put("_type", _type);
        if (rateCustom != null) {
            buyParams.put("ratecustom", rateCustom);
        }

        if (rate != null) {
            buyParams.put("rate", rate);
        }

        if (customerType != null) {
            buyParams.put("customerType", customerType);
        }

        Map<String, Object> sellParams = new HashMap<>();
        sellParams.put("productcode", productCode);
        sellParams.put("selldate", calculateDate);
        sellParams.put("quantity", quantity);
        sellParams.put("pitfee", pitFee);
        sellParams.put("purpose", purpose);
        sellParams.put("_type", "2");
        sellParams.put("buydatecustom", buyDateCustom);
        sellParams.put("buypricecustom", buyPriceCustom);
        sellParams.put("buyvolumecustom", buyVolumeCustom);

        if (orderId != null) {
            sellParams.put("orderid", orderId);
        }

        if (issuerCall != null) {
            sellParams.put("issuercall", issuerCall);
        }
//        System.out.println(calculateDate + "----------------------" + buyParams);
        try {
            System.out.println("BUY PARAMS" + calculateDate + "----------------------" + buyParams);
            HttpResponse<JsonNode> jsonBuyResponse = Unirest.post(urlPricingService).headers(headers).fields(buyParams)
                    .asJson();

            JSONObject responseBuyObject = jsonBuyResponse.getBody().getObject();
            System.out.println(calculateDate + "Response BUY Request----------------------" + responseBuyObject);
            if (responseBuyObject.getInt("status") == 0) {
                JSONObject jsonBuyObj = responseBuyObject.getJSONObject("data");
//                System.out.println(calculateDate + "Response Buy Request----------------------" + jsonBuyObj);
                try {
                    if (sellFor.equals("1")) {
                        sellParams.put("sellPriceCustom", jsonBuyObj.getString("buyprice"));
                    } else if (sellFor.equals("2")) {
                        sellParams.put("sellPriceCustom", jsonBuyObj.getString("buyPriceTCBS"));
                    }
                    System.out.println("SELL PARAMS" + calculateDate + "----------------------" + sellParams);
                    HttpResponse<JsonNode> jsonSellResponse = Unirest.post(urlPricingService).headers(headers).fields(sellParams)
                            .asJson();
                    JSONObject responseSellObject = jsonSellResponse.getBody().getObject();
                    JSONObject jsonSellObj = responseSellObject.getJSONObject("data");
                    System.out.println(calculateDate + "Response Sell Request----------------------" + jsonSellObj);
                    if (responseSellObject.getInt("status") == 0) {
                        Map<String, Object> jsonMapSell = JsonMapConverter.toMap(jsonSellObj);
                        jsonMapSell.put("interestBuyer",
                                Double.parseDouble(jsonBuyObj.getString("investmentRateWithReCoupon")));
                        buyParams.clear();
                        sellParams.clear();
                        return new JSONObject(jsonMapSell);
                    } else {
                        Map<String, String> jsonObjError = new HashMap<>();
                        jsonObjError.put("errorMessage", responseSellObject.getString("data"));
                        buyParams.clear();
                        sellParams.clear();
                        return new JSONObject(jsonObjError);
                    }
                } catch (UnirestException e) {
                    e.printStackTrace();
                }
            } else {
                Map<String, String> jsonObjError = new HashMap<>();
                jsonObjError.put("errorMessage", responseBuyObject.getString("data"));
                return new JSONObject(jsonObjError);
            }
        } catch (UnirestException e) {
            e.printStackTrace();
        }
        return null;
    }

    @Override
    public JSONObject getYieldCurveInfo() throws JSONException, IOException {
        String url = urlDSSStarwarService + contextDSSGetYieldCurveInfo;
        Map<String, String> headers = new HashMap<>();
        headers.put("Content-Type", "application/x-www-form-urlencoded");
        headers.put("Accept", "application/json");
        try {
            HttpResponse<JsonNode> jsonResponse = Unirest.get(url).headers(headers).asJson();
            JSONObject responseObject = jsonResponse.getBody().getObject();
            if (responseObject.has("entries")) {
                JSONArray advisorJsonArrayResp = responseObject.getJSONObject("entries").getJSONArray("entry");
                List<JSONObject> advisorArray = new ArrayList<>();
                for (int i = 0; i < advisorJsonArrayResp.length(); i++) {
                    Map<String, Object> yieldCurveItem = new HashMap<>();
                    yieldCurveItem.put("rate", advisorJsonArrayResp.getJSONObject(i).isNull("rate") ? null:advisorJsonArrayResp.getJSONObject(i).getDouble("rate"));
                    System.out.print(advisorJsonArrayResp.getJSONObject(i).isNull("rateMDN"));
                    yieldCurveItem.put("rateMDN", advisorJsonArrayResp.getJSONObject(i).isNull("rateMDN")? null:advisorJsonArrayResp.getJSONObject(i).getDouble("rateMDN"));
                    yieldCurveItem.put("a", advisorJsonArrayResp.getJSONObject(i).isNull("a")? null:advisorJsonArrayResp.getJSONObject(i).getDouble("a"));
                    yieldCurveItem.put("b", advisorJsonArrayResp.getJSONObject(i).isNull("b")? null:advisorJsonArrayResp.getJSONObject(i).getDouble("b"));
                    yieldCurveItem.put("maturity_m", advisorJsonArrayResp.getJSONObject(i).isNull("maturity_m")? null:advisorJsonArrayResp.getJSONObject(i).getInt("maturity_m"));
                    yieldCurveItem.put("rateMDNINT", advisorJsonArrayResp.getJSONObject(i).isNull("rateMDNINT")? null:advisorJsonArrayResp.getJSONObject(i).getDouble("rateMDNINT"));
                    yieldCurveItem.put("applyType", advisorJsonArrayResp.getJSONObject(i).isNull("applyType")? null:advisorJsonArrayResp.getJSONObject(i).getString("applyType"));
                    yieldCurveItem.put("issuerGroup", advisorJsonArrayResp.getJSONObject(i).isNull("issuerGroup")? null:advisorJsonArrayResp.getJSONObject(i).getString("issuerGroup"));
                    advisorArray.add(new JSONObject(yieldCurveItem));
                }
                Map<String, Object> yieldCurveInfo = new HashMap<>();
                yieldCurveInfo.put("data", advisorArray);
                return new JSONObject(yieldCurveInfo);
            }
        } catch (UnirestException e) {
            e.printStackTrace();
        }

        return null;
    }

    //    @Override
//    public Observable<String> getRxString() {
//        return Observable.just("test RX method");
//    }
}
