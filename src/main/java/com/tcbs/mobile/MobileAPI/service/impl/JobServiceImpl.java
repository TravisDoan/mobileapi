//package com.tcbs.mobile.MobileAPI.service.impl;
//
//import com.mashape.unirest.http.HttpResponse;
//import com.mashape.unirest.http.JsonNode;
//import com.mashape.unirest.http.Unirest;
//import com.mashape.unirest.http.exceptions.UnirestException;
//import com.tcbs.mobile.MobileAPI.entity.BondInfo;
//import com.tcbs.mobile.MobileAPI.service.base.JobService;
//import com.tcbs.mobile.MobileAPI.utils.GeneralUtils;
//import org.json.JSONArray;
//import org.json.JSONException;
//import org.json.JSONObject;
//import org.springframework.beans.factory.annotation.Value;
//import org.springframework.stereotype.Service;
//
//import java.io.FileWriter;
//import java.io.IOException;
//import java.util.*;
//
//@Service
//public class JobServiceImpl implements JobService {
//
//    @Value("${bond.info.file}")
//    private String urlSaveFile;
//
//    @Value("${url.esb.pricing.service}")
//    private String urlPricingService;
//
//    @Value("${url.dss.invest.service}")
//    private String urlDSSInvestService;
//
//    @Value("${url.dss.pricing.engine.service}")
//    private String urlDSSPricingEngineService;
//
//    @Value("${url.dss.invest2.service}")
//    private String urlDSSInvest2Service;
//
//    @Value("${url.dss.bond.service}")
//    private String urlDSSBondOldService;
//
//    @Value("${context.dss.get.bond.getIAdvisorBondInfo}")
//    private String contextDSSGetIAdvisorBondInfo;
//
//    @Value("${context.dss.post.invest.insertSalesKitBondInfo}")
//    private String contextDSSInsertSalesKitBondInfo;
//
//    @Value("${context.dss.post.invest.updateMaturityHoldRate}")
//    private String contextDSSUpdateMHRSalesKitBondInfo;
//
//    @Value("${context.dss.get.invest.deleteOldDataSalesKitBondInfo}")
//    private String contextDSSDeleteOldDataSalesKitBondInfo;
//
//    @Value("${context.dss.get.invest.bondhaspricing}")
//    private String contextDSSGetSalesKitProduct;
//
//    @Value("${context.dss.get.invest.bondinfo}")
//    private String contextDSSGetBondInfo;
//
//    // GET SERVICES
//
//    @Override
//    public Map<String, Object> jobUpdateSalesKitBondInfoFromTCBondOld() throws JSONException, IOException {
//        String urlBondOld = urlDSSBondOldService + contextDSSGetIAdvisorBondInfo;
//        String urlInvest = urlDSSInvest2Service + contextDSSInsertSalesKitBondInfo;
//        Map<String, String> headers = new HashMap<>();
//        headers.put("Content-Type", "application/x-www-form-urlencoded");
//        headers.put("Accept", "application/json");
//        try {
//
//            HttpResponse<JsonNode> jsonResponse = Unirest.get(urlBondOld)
//                    .headers(headers)
//                    .asJson();
//
//            JSONObject responseObject = jsonResponse.getBody().getObject();
//            Map<String, Object> responseJson = new HashMap<>();
//            if (responseObject.has("entries")) {
//                List<BondInfo> bondData = new ArrayList<>();
//                List<Map<String, Object>> listProcessResponse = new ArrayList<>();
//
//                JSONArray jArray = responseObject.getJSONObject("entries").getJSONArray("entry");
//                for (int i = 0; i < jArray.length(); i++) {
//                    JSONObject bondInfoObj = (JSONObject) jArray.get(i);
//                    bondData.add(new BondInfo(
//                            bondInfoObj.isNull("productID") ? 0 : bondInfoObj.getInt("productID"),
//                            bondInfoObj.isNull("productCode") ? "" : bondInfoObj.getString("productCode"),
//                            bondInfoObj.isNull("bondID") ? 0 : bondInfoObj.getInt("bondID"),
//                            bondInfoObj.isNull("bondCode") ? "" : bondInfoObj.getString("bondCode"),
//                            bondInfoObj.isNull("categoryID") ? 0 : bondInfoObj.getInt("categoryID"),
//                            bondInfoObj.isNull("category") ? "" : bondInfoObj.getString("category"),
//                            bondInfoObj.isNull("par") ? 0.0 : bondInfoObj.getDouble("par"),
//                            bondInfoObj.isNull("issueDate") ? "" : bondInfoObj.getString("issueDate"),
//                            bondInfoObj.isNull("maturityDate") ? "" : bondInfoObj.getString("maturityDate"),
//                            bondInfoObj.isNull("noteType") ? "" : bondInfoObj.getString("noteType"),
//                            bondInfoObj.isNull("noteInterest") ? "" : bondInfoObj.getString("noteInterest"),
//                            bondInfoObj.isNull("notePayInterest") ? "" : bondInfoObj.getString("notePayInterest"),
//                            bondInfoObj.isNull("issuerName") ? "" : bondInfoObj.getString("issuerName"),
//                            bondInfoObj.isNull("issuerInfoUrl") ? "" : bondInfoObj.getString("issuerInfoUrl"),
//                            bondInfoObj.isNull("availableForSale") ? 0 : bondInfoObj.getInt("availableForSale"),
//                            bondInfoObj.isNull("maturityHoldRate") ? 0.0 : bondInfoObj.getDouble("maturityHoldRate"),
//                            bondInfoObj.isNull("isCallable") ? "" : bondInfoObj.getString("isCallable")));
//                }
//
//                for (BondInfo item : bondData) {
//                    Map<String, Object> params = new HashMap<>();
//                    params.put("productId", item.getProductId());
//                    params.put("productCode", item.getProductCode());
//                    params.put("bondId", item.getBondId());
//                    params.put("bondCode", item.getBondCode());
//                    params.put("categoryId", item.getCategoryId());
//                    params.put("category", item.getCategory());
//                    params.put("par", item.getPar());
//                    params.put("issueDate", item.getIssueDate());
//                    params.put("maturityDate", item.getMaturityDate());
//                    params.put("noteType", item.getNoteType());
//                    params.put("noteInterest", item.getNoteInterest());
//                    params.put("notePayInterest", item.getNotePayInterest());
//                    params.put("issuerName", item.getIssuerName());
//                    params.put("issuerInfoUrl", item.getIssuerInfoUrl());
//                    params.put("availableForSale", item.getAvailableForSale());
//                    params.put("maturityHoldRate", item.getMaturityHoldRate());
//                    params.put("isCallable", item.getIsCallable());
//                    Map<String, Object> processResponse = new HashMap<>();
//
//
//                    HttpResponse<JsonNode> jsonResp = Unirest.post(urlInvest)
//                            .headers(headers)
//                            .fields(params)
//                            .asJson();
//                    JSONObject responseObj = jsonResp.getBody().getObject();
//                    if (responseObj.has("data")) {
//                        JSONObject respChildObj = responseObj.getJSONObject("data");
//                        if (!respChildObj.isNull("ID")) {
//                            processResponse.put(item.getProductCode(), "SUCCESS");
//                            listProcessResponse.add(processResponse);
//                        } else {
//                            processResponse.put(item.getProductCode(), "FAIL");
//                            listProcessResponse.add(processResponse);
//                        }
//                    }
//                }
//                responseJson.put("updateStatus", listProcessResponse);
//
//                return responseJson;
//            } else {
//                Map<String, Object> jsonObjError = new HashMap<>();
//                jsonObjError.put("errorMessage", responseObject.getString("data"));
//
//                return jsonObjError;
//            }
//        } catch (UnirestException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    @Override
//    public JSONObject jobDeleteOldDataSalesKitBondInfoTCInvest() throws JSONException, IOException {
//        String urlInvest = urlDSSInvest2Service + contextDSSDeleteOldDataSalesKitBondInfo;
//        Map<String, String> headers = new HashMap<>();
//        headers.put("Content-Type", "application/x-www-form-urlencoded");
//        headers.put("Accept", "application/json");
//        try {
//
//            HttpResponse<JsonNode> jsonResponse = Unirest.get(urlInvest)
//                    .headers(headers)
//                    .asJson();
//            JSONObject responseObject = jsonResponse.getBody().getObject();
//
//            return responseObject.getJSONObject("data");
//        } catch (UnirestException e) {
//            e.printStackTrace();
//        }
//        return null;
//    }
//
//    @Override
//    public Map<String, Object> jobWrappingSalesKitBondInfoForIAdvisorAndSaveToFile() throws JSONException, IOException {
//        String urlInvest = urlDSSPricingEngineService + contextDSSGetSalesKitProduct;
//        String urlBond = urlDSSInvestService + contextDSSGetBondInfo;
//        Map<String, String> headers = new HashMap<>();
//        headers.put("Content-Type", "application/x-www-form-urlencoded");
//        headers.put("Accept", "application/json");
//
//        try {
//            List<String> advisorArray = new ArrayList<>();
//            List<String> categoryAdvisorArray = new ArrayList<>();
//
//
//            HttpResponse<JsonNode> advisorJsonResponse = Unirest.get(urlInvest)
//                    .headers(headers)
//                    .asJson();
//
//            JSONObject advisorJsonObject = advisorJsonResponse.getBody().getObject();
//            if (advisorJsonObject.has("entries")) {
//                JSONArray advisorJsonArrayResp = advisorJsonObject.getJSONObject("entries").getJSONArray("entry");
//                for (int i = 0; i < advisorJsonArrayResp.length(); i++) {
//                    advisorArray.add(advisorJsonArrayResp.getJSONObject(i).getString("productCode"));
////                    System.out.println("HasPricing"+advisorJsonArrayResp.getJSONObject(i).getString("productCode"));
//                }
//                for (String advisorArrayItem : advisorArray) {
//                    String categoryAdvisor = advisorArrayItem.substring(0, advisorArrayItem.indexOf("-"));
//                    if (categoryAdvisorArray.indexOf(categoryAdvisor) == -1) {
//                        categoryAdvisorArray.add(categoryAdvisor);
//                    }
//                }
//
//                List<JSONObject> activeBondInfoArray = new ArrayList<>();
//                List<JSONObject> dataBondInfoArray = new ArrayList<>();
//                try {
//
//
//                    HttpResponse<JsonNode> bondInfoJsonResponse = Unirest.get(urlBond)
//                            .headers(headers)
//                            .asJson();
//                    JSONObject bondInfoJsonObject = bondInfoJsonResponse.getBody().getObject();
//                    if (bondInfoJsonObject.has("entries")) {
//                        JSONArray bondInfoJsonArrayResp = bondInfoJsonObject.getJSONObject("entries").getJSONArray("entry");
//                        for (int i = 0; i < bondInfoJsonArrayResp.length(); i++) {
//                            for (String advisorArrayItem : advisorArray) {
//                                if (bondInfoJsonArrayResp.getJSONObject(i).getString("productCode").equals(advisorArrayItem)) {
////                                	System.out.println("BondInfo"+bondInfoJsonArrayResp.getJSONObject(i).getString("productCode"));
//                                    if (!bondInfoJsonArrayResp.getJSONObject(i).isNull("maturityHoldRate")) {
//                                        activeBondInfoArray.add(bondInfoJsonArrayResp.getJSONObject(i));
//                                    }
//                                }
//                            }
//                        }
//                        for (String categoryAdvisorArrayItem : categoryAdvisorArray) {
//                            Map<String, Object> categoryJsonObject = new HashMap<>();
//                            List<JSONObject> categoryJsonArray = new ArrayList<>();
//                            activeBondInfoArray.sort(new Comparator<JSONObject>() {
//                                @Override
//                                public int compare(JSONObject o1, JSONObject o2) {
//                                    try {
//                                        return Integer.parseInt(o2.getString("availableForSale").replace(".0", "")) - Integer.parseInt(o1.getString("availableForSale").replace(".0", ""));
//                                    } catch (JSONException e) {
//                                        e.printStackTrace();
//                                    }
//                                    return 0;
//                                }
//                            });
//
//                            for (JSONObject activeBondInfoArrayItem : activeBondInfoArray) {
//                                if (activeBondInfoArrayItem.getString("category").equals(categoryAdvisorArrayItem)) {
//                                    categoryJsonObject.put("categoryID", activeBondInfoArrayItem.getString("categoryID").replace(".", "").replace("0", ""));
//                                    categoryJsonObject.put("categoryName", activeBondInfoArrayItem.getString("category"));
//                                    Map<String, Object> childCategoryJsonObject = new HashMap<>();
//                                    childCategoryJsonObject.put("productCODE", activeBondInfoArrayItem.getString("productCode"));
//                                    childCategoryJsonObject.put("noteInterest", activeBondInfoArrayItem.getString("noteInterest"));
//                                    childCategoryJsonObject.put("expiredDate", GeneralUtils.getCurrenDate(activeBondInfoArrayItem.getString("maturityDate"), GeneralUtils.DateType.NORMAL));
//                                    childCategoryJsonObject.put("expiredDateIOS", GeneralUtils.getCurrenDate(activeBondInfoArrayItem.getString("maturityDate"), GeneralUtils.DateType.IOS));
//                                    childCategoryJsonObject.put("createdDate", GeneralUtils.getCurrenDate(activeBondInfoArrayItem.getString("issueDate"), GeneralUtils.DateType.NORMAL));
//                                    childCategoryJsonObject.put("createdDateIOS", GeneralUtils.getCurrenDate(activeBondInfoArrayItem.getString("issueDate"), GeneralUtils.DateType.IOS));
////                                    childCategoryJsonObject.put("productId", activeBondInfoArrayItem.getString("productID"));
//                                    childCategoryJsonObject.put("notePayInterest", activeBondInfoArrayItem.getString("notePayInterest"));
//                                    childCategoryJsonObject.put("availableForSale", (double) Math.round(Double.parseDouble(activeBondInfoArrayItem.getString("availableForSale")) * Double.parseDouble(activeBondInfoArrayItem.getString("par")) / 1000000000 * 10) / 10);
//                                    childCategoryJsonObject.put("maturityHoldRate", activeBondInfoArrayItem.getString("maturityHoldRate"));
//                                    childCategoryJsonObject.put("bondCode", activeBondInfoArrayItem.getString("bondCode"));
//                                    childCategoryJsonObject.put("par", activeBondInfoArrayItem.getString("par"));
//                                    if (!activeBondInfoArrayItem.isNull("issuerInfoUrl")) {
//                                        childCategoryJsonObject.put("issuerInfoUrl", activeBondInfoArrayItem.getString("issuerInfoUrl"));
//                                    } else {
//                                        childCategoryJsonObject.put("issuerInfoUrl", "");
//                                    }
//                                    childCategoryJsonObject.put("isCallable", activeBondInfoArrayItem.getString("isCallable"));
//                                    childCategoryJsonObject.put("noteType", activeBondInfoArrayItem.getString("noteType"));
//                                    childCategoryJsonObject.put("issuerName", activeBondInfoArrayItem.getString("issuerName"));
//
//                                    if (Double.parseDouble(activeBondInfoArrayItem.getString("maturityHoldRate")) != 0.0) {
//                                        categoryJsonArray.add(new JSONObject(childCategoryJsonObject));
//                                    }
//
//                                }
//                            }
//                            categoryJsonObject.put("child", categoryJsonArray);
//                            dataBondInfoArray.add(new JSONObject(categoryJsonObject));
//                        }
//                        GeneralUtils.saveJSONToFile(dataBondInfoArray);
////                        }
//                        if (dataBondInfoArray.size() > 0) {
//                            Map<String, Object> dataBondInfoMap = new HashMap<>();
//                            dataBondInfoMap.put("bondInfo", "Generate Data For IAdvisor Successful!");
//
//                            return dataBondInfoMap;
//                        } else {
//                            Map<String, Object> dataBondInfoMap = new HashMap<>();
//                            dataBondInfoMap.put("bondInfo", "Generate Data For IAdvisor Fail!");
//
//                            return dataBondInfoMap;
//                        }
//                    }
//
//                } catch (UnirestException e) {
//                    e.printStackTrace();
//                    Map<String, Object> jsonObjError = new HashMap<>();
//                    jsonObjError.put("errorMessage", e);
//
//                    return jsonObjError;
//                }
//
//            } else {
//                Map<String, Object> jsonObjError = new HashMap<>();
//                jsonObjError.put("errorMessage", advisorJsonObject.getString("data"));
//
//                return jsonObjError;
//            }
//        } catch (UnirestException e) {
//            e.printStackTrace();
//            Map<String, Object> jsonObjError = new HashMap<>();
//            jsonObjError.put("errorMessage", e);
//
//            return jsonObjError;
//        }
//        return null;
//    }
//
//    @Override
//    public Map<String, Object> jobUpdateMaturityHoldRateForSalesKitBondInfoTCInvest() throws JSONException, IOException {
//
//        String urlInvest = urlDSSInvestService + contextDSSGetSalesKitProduct;
//        String urlMHR = urlDSSInvest2Service + contextDSSUpdateMHRSalesKitBondInfo;
//        Map<String, String> headers = new HashMap<>();
//        headers.put("Content-Type", "application/x-www-form-urlencoded");
//        headers.put("Accept", "application/json");
//
//        try {
//            List<String> advisorArray = new ArrayList<>();
//            List<String> categoryAdvisorArray = new ArrayList<>();
//
//
//            HttpResponse<JsonNode> advisorJsonResponse = Unirest.get(urlInvest)
//                    .headers(headers)
//                    .asJson();
//
//            JSONObject advisorJsonObject = advisorJsonResponse.getBody().getObject();
//            Map<String, Object> responseJson = new HashMap<>();
//            if (advisorJsonObject.has("entries")) {
//                JSONArray advisorJsonArrayResp = advisorJsonObject.getJSONObject("entries").getJSONArray("entry");
//                List<Map<String, Object>> listUpdateMHRResponse = new ArrayList<>();
//                Map<String, Object> updateMRHResponse = new HashMap<>();
//                for (int i = 0; i < advisorJsonArrayResp.length(); i++) {
//                    Map<String, Object> params = new HashMap<>();
//                    params.put("productcode", advisorJsonArrayResp.getJSONObject(i).getString("productCode"));
//                    params.put("buydate", GeneralUtils.getToday(new Date(), "dd/MM/yyyy"));
//                    params.put("quantity", 2000);
//                    params.put("purpose", "SALEKIT");
//                    params.put("_type", 1);
//
//                    try {
//
//
//                        HttpResponse<JsonNode> jsonResponse = Unirest.post(urlPricingService)
//                                .headers(headers)
//                                .fields(params)
//                                .asJson();
//
//                        JSONObject responseObject = jsonResponse.getBody().getObject();
////                        System.out.println(jsonResponse);
////                        System.out.println(responseObject.getInt("status"));
////                        System.out.println(responseObject.getString("msg"));
////                        System.out.println(advisorJsonArrayResp.getJSONObject(i).getString("productCode"));
//                        if (responseObject.getInt("status") == 0) {
//                            JSONObject dataObjResponse = responseObject.getJSONObject("data");
//                            if (dataObjResponse.isNull("investmentRateWithReCoupon")) {
//                                updateMRHResponse.put(advisorJsonArrayResp.getJSONObject(i).getString("productCode"), "InvestmentRateWithReCoupon not exist!");
//                            } else {
//                                try {
//                                    Map<String, Object> paramsUpdateMHR = new HashMap<>();
//                                    paramsUpdateMHR.put("maturityHoldRate", Double.parseDouble(GeneralUtils.formatWithTwoCommas(dataObjResponse.getDouble("investmentRateWithReCoupon") * 100)));
//                                    paramsUpdateMHR.put("productCode", advisorJsonArrayResp.getJSONObject(i).getString("productCode"));
//                                    HttpResponse<JsonNode> jsonUpdateMHRResponse = Unirest.post(urlMHR)
//                                            .headers(headers)
//                                            .fields(paramsUpdateMHR)
//                                            .asJson();
//                                    JSONObject jsonUpdateMHR = jsonUpdateMHRResponse.getBody().getObject();
//                                    if (jsonUpdateMHR.getJSONObject("UpdatedRowCount").getInt("Value") == 0) {
//                                        updateMRHResponse.put(advisorJsonArrayResp.getJSONObject(i).getString("productCode"), "Update Fail!");
//                                    } else {
//                                        updateMRHResponse.put(advisorJsonArrayResp.getJSONObject(i).getString("productCode"), "Update Success!");
//                                    }
//                                } catch (UnirestException e) {
//                                    e.printStackTrace();
//                                    updateMRHResponse.put(advisorJsonArrayResp.getJSONObject(i).getString("productCode"), e);
//                                }
//                            }
//                        } else {
//                            updateMRHResponse.put(advisorJsonArrayResp.getJSONObject(i).getString("productCode"), responseObject.getString("data"));
//                        }
//                    } catch (UnirestException e) {
//                        e.printStackTrace();
//                        updateMRHResponse.put(advisorJsonArrayResp.getJSONObject(i).getString("productCode"), e);
//                    }
//                }
//                listUpdateMHRResponse.add(updateMRHResponse);
//                responseJson.put("Update Maturity Hold Rate Status", listUpdateMHRResponse);
//
//                return responseJson;
//            }
//        } catch (UnirestException e) {
//            e.printStackTrace();
//            Map<String, Object> jsonObjError = new HashMap<>();
//            jsonObjError.put("errorMessage", e);
//
//            return jsonObjError;
//        }
//        return null;
//    }
//}
