package com.tcbs.mobile.MobileAPI.utils;

import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.time.format.DateTimeFormatter;
import java.time.temporal.ChronoField;
import java.time.temporal.TemporalAccessor;
import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.stream.Collectors;

import org.json.simple.JSONObject;
import org.json.simple.parser.JSONParser;
import org.json.simple.parser.ParseException;
import org.springframework.beans.factory.annotation.Value;

public class GeneralUtils {

    public enum DateType {
        NORMAL,
        IOS
    }

    public static DecimalFormat df2 = new DecimalFormat(".##");
    public static DecimalFormat df = new DecimalFormat(".#");

    public static String getCurrenDate(String dateStr, DateType type) {
        DateTimeFormatter timeFormatter = DateTimeFormatter.ISO_DATE_TIME;
        TemporalAccessor accessor = timeFormatter.parse(dateStr);
//        Date date = Date.from(Instant.from(accessor));
        String day = String.valueOf(accessor.get(ChronoField.DAY_OF_MONTH));
        String month = String.valueOf(accessor.get(ChronoField.MONTH_OF_YEAR));
        String year = String.valueOf(accessor.get(ChronoField.YEAR));
        if ( accessor.get(ChronoField.DAY_OF_MONTH) < 10) {
            day = "0" + day;
        }
        if (accessor.get(ChronoField.MONTH_OF_YEAR) < 10) {
            month = "0" + month;
        }
        if (type == DateType.NORMAL) {
            return day+"/"+month+"/"+year;
        } else {
            return month+"/"+day+"/"+year;
        }
    }

    public static String getToday(Date dateToday, String format) {
        return new SimpleDateFormat(format).format(dateToday);
    }

    public static String formatWithTwoCommas(double number) {
        return df2.format(number);
    }

    public static String formatWithOneCommas(double number) {
        return df.format(number);
    }

    public static Map<String, Object> saveJSONToFile(List<org.json.JSONObject> dataBondInfoArray) {
        String urlWriteFile = Constants.urlBondInfoFile;
        try (FileWriter file = new FileWriter(urlWriteFile)) {
            Map<String, Object> dataBondInfoMap = new HashMap<>();
            dataBondInfoMap.put("bondInfo", dataBondInfoArray);
            file.write(new JSONObject(dataBondInfoMap).toJSONString());
//            System.out.println("Successfully Copied JSON Object to File...");
//            System.out.println("\nJSON Object: " + new Date().toString() + dataBondInfoMap);
        } catch (IOException e) {
            e.printStackTrace();
            Map<String, Object> jsonObjError = new HashMap<>();
            jsonObjError.put("errorMessage", e);
            return jsonObjError;
        }
        return null;
    }
    
    public static String readJSONFromFile() {
        String urlReadFile = Constants.urlBondInfoFile;
        JSONParser parser = new JSONParser();
        Object obj = null;
        try {
            obj = parser.parse(new FileReader(
                    urlReadFile));
            JSONObject jsonObject = (JSONObject) obj;
            return jsonObject.toJSONString();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return null;
    }
    
    public static CompletableFuture<Map<String, Object>> calculateFuturePricing() {

        return null;
    }


//    public static void main(String[] args) {
//        System.out.println(getCurrenDate("2017-08-28T07:00:00.000+07:00", DateType.IOS));
//        List<String> arrayDate = Arrays.asList("17/11/2017", "17/12/2017", "17/1/2018");
//        String strDate = String.join(",", arrayDate);
//        System.out.println(strDate);
//        String[] ary = strDate.split(",");
//        List<String> strings = Arrays.asList(ary);
//        for (int i = 0; i < strings.size(); i++) {
//            System.out.println(strings.get(i));
//        }
//        System.out.println(ary.toString());
//    }
}
