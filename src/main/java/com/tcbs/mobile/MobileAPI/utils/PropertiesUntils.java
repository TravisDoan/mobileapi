package com.tcbs.mobile.MobileAPI.utils;

import java.io.InputStream;
import java.util.Properties;

public class PropertiesUntils {

	private static PropertiesUntils instance;

	public static PropertiesUntils getInstance() {
		if(instance == null) {
			instance = new PropertiesUntils();
		}
		return instance;
	}
	
	
	public String getProperties(String key) {
		try {
			Properties prop = new Properties();
			String propFileName = "configs.properties";
			InputStream inputStream = getClass().getClassLoader().getResourceAsStream(propFileName);
			if (inputStream != null) {
				prop.load(inputStream);
				String data = prop.getProperty(key);
				return data;
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		return null;
	}
}
