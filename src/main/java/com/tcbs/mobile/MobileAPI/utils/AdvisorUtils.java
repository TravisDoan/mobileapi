package com.tcbs.mobile.MobileAPI.utils;

import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.JsonNode;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;
import com.tcbs.mobile.MobileAPI.service.impl.AdvisorServiceImpl;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.security.KeyManagementException;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;

import javax.net.ssl.SSLContext;
import javax.security.cert.CertificateException;
import javax.security.cert.X509Certificate;

import org.apache.http.client.HttpClient;
import org.apache.http.conn.ssl.NoopHostnameVerifier;
import org.apache.http.conn.ssl.TrustSelfSignedStrategy;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.ssl.SSLContextBuilder;


import java.util.*;
import java.util.concurrent.CompletableFuture;
import java.util.function.Supplier;

public class AdvisorUtils {

    public static Map<String, Object> putObjectDataToMap(Object obj, JSONObject jsonObj, String key, Map<String, Object> map) throws JSONException {
        if (obj instanceof Integer) {
            Integer value = jsonObj.getInt(key);
            map.put(key, value);
        } else if (obj instanceof Double) {
            Double value = jsonObj.getDouble(key);
            map.put(key, value);
        } else if (obj instanceof String) {
            String value = jsonObj.getString(key);
            map.put(key, value);
        }
        return map;
    }

    public static Map<String, Object> putJSONArrayDataToMap(Object obj, JSONArray jArray, String key, Map<String, Object> map) throws JSONException {
        if (obj instanceof JSONObject) {
            List<Map<String, Object>> listMap = new ArrayList<>();
            for (int i = 0; i < jArray.length(); i++) {
                Map<String, Object> subMap = new HashMap<>();
                JSONObject keyObject = jArray.getJSONObject(i);
                Iterator iterSubKey = keyObject.keys();
                while (iterSubKey.hasNext()) {
                    String subKey = (String) iterSubKey.next();
                    Object objValue = keyObject.get(subKey);
                    subMap = AdvisorUtils.putObjectDataToMap(objValue, keyObject, subKey, subMap);
                }
                listMap.add(subMap);
            }
            map.put(key, listMap);
        } else if (obj instanceof String) {
            List<String> listMap = new ArrayList<>();
            for (int i = 0; i < jArray.length(); i++) {
                String keyStr = jArray.getString(i);
                listMap.add(keyStr);
            }
            map.put(key, listMap);
        } else if (obj instanceof Integer) {
            List<Integer> listMap = new ArrayList<>();
            for (int i = 0; i < jArray.length(); i++) {
                Integer keyStr = jArray.getInt(i);
                listMap.add(keyStr);
            }
            map.put(key, listMap);
        } else if (obj instanceof Double) {
            List<Double> listMap = new ArrayList<>();
            for (int i = 0; i < jArray.length(); i++) {
                Double keyStr = jArray.getDouble(i);
                listMap.add(keyStr);
            }
            map.put(key, listMap);
        }

        return map;
    }

    public static CompletableFuture<Map<String, Object>> calculateSellBeforeExpired(
            List<String> arrayFutureDate,
            String productCode,
            String buyDate,
            String quantity,
            String pitFee,
            String purpose,
            String _type,
            String rateCustom,
            String buyPrice,
            String monthsLeft,
            String calculateDate) {
        return CompletableFuture.supplyAsync(new Supplier<Map<String, Object>>() {
            @Override
            public Map<String, Object> get() {
                try {
                    Map<String, Object> multiDateResponse = new HashMap<>();
                    int index = arrayFutureDate.indexOf(calculateDate);
                    Map<String, String> headers = new HashMap<>();
                    headers.put("Content-Type", "application/x-www-form-urlencoded");
                    headers.put("Accept", "application/json");
                    Map<String, Object> params = new HashMap<>();
                    params.put("productcode", productCode);
                    params.put("buydate", calculateDate.replace("\"", ""));
                    params.put("quantity", quantity);
                    params.put("pitfee", pitFee);
                    params.put("purpose", purpose);
                    params.put("_type", _type);
//                    System.out.print(productCode);
                    if (rateCustom != null) {
                        params.put("ratecustom", rateCustom);
                    }
                    try {
                        HttpResponse<JsonNode> jsonResponse = Unirest.post("https://iadvisor.tcbs.com.vn/api/product")
                                .headers(headers)
                                .fields(params)
                                .asJson();

                        JSONObject responseObject = jsonResponse.getBody().getObject();
                        if (responseObject.getInt("status") == 0) {
                            JSONObject jsonBuyObj = responseObject.getJSONObject("data");

                            Map<String, Object> paramsSell = new HashMap<>();
                            paramsSell.put("productcode", productCode);
                            paramsSell.put("selldate", calculateDate.replace("\"", ""));
                            paramsSell.put("quantity", quantity);
                            paramsSell.put("pitfee", pitFee);
                            paramsSell.put("purpose", purpose);
                            paramsSell.put("_type", 2);
                            paramsSell.put("buydatecustom", buyDate);
                            paramsSell.put("buypricecustom", quantity);
                            paramsSell.put("buyvolumecustom", buyPrice);
                            paramsSell.put("sellPriceCustom", jsonBuyObj.getString("total"));

                            try {
                                HttpResponse<JsonNode> jsonSellResponse = Unirest.post("https://iadvisor.tcbs.com.vn/api/product")
                                        .headers(headers)
                                        .fields(params)
                                        .asJson();

                                JSONObject responseSellObject = jsonResponse.getBody().getObject();
                                if (responseSellObject.getInt("status") == 0) {
                                    Map<String, Object> jsonMapSell = JsonMapConverter.toMap(responseSellObject.getJSONObject("data"));
                                    jsonMapSell.put("interestBuyer", Double.parseDouble(jsonBuyObj.getString("investmentRateWithReCoupon")));
                                    jsonMapSell.put("globalIndex", Integer.parseInt(monthsLeft) - (index + 1));
                                    multiDateResponse = jsonMapSell;
                                    if (responseSellObject.getJSONObject("data").getJSONArray("couponreceivedArr").length() > 0) {
                                        multiDateResponse.put("investRateBeforeExpire", jsonMapSell.get("investmentRate"));
                                        multiDateResponse.put("reInvestRateBeforeExpire", jsonMapSell.get("investmentRateWithReinvest"));
                                    } else {
                                        multiDateResponse.put("investRateBeforeExpire", jsonMapSell.get("investmentRate"));
                                        multiDateResponse.put("reInvestRateBeforeExpire", jsonMapSell.get("investmentRate"));
                                    }

                                    return multiDateResponse;
                                }
//                                else {
//                                    Map<String, String> jsonSellObjError = new HashMap<>();
//                                    jsonSellObjError.put("errorMessage", responseObject.getString("data"));
//                                    return new JSONObject(jsonSellObjError);
//                                }
                            } catch (UnirestException e) {

                                e.printStackTrace();
                            }

                        } else {
                            Map<String, String> jsonObjError = new HashMap<>();
                            jsonObjError.put("errorMessage", responseObject.getString("data"));
                            JSONObject jsonErrorObj = new JSONObject(jsonObjError);
                        }
                    } catch (UnirestException e) {
                        e.printStackTrace();
                    }
                    return null;

                } catch (Throwable e) {
                    e.printStackTrace();
                    return null;
                }
            }
        });


    }
}
