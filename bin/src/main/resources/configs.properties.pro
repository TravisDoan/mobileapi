#BOND URL DSS

#mode_data_base
tcbond.db.mode=1

product.code.online=TCBondPrix-VINSTAFF;


#url.bond.service=http://10.8.0.46:9766/services/mssql_tcinvest2/
url.bond.service=http://10.8.0.161:9767/services/invest_migrate_bond/

#DEV
#url.old.bond.service=http://10.8.0.161:9767/services/tcbond_old/

#PRO
url.old.bond.service=http://10.98.2.99:9767/services/tcbond_old/

context.post.old.pricing=get-pricing-data
context.get.old.rate.data=get-rate-by-id/
context.get.old.bond.attr=bond-attribute
context.get.old.bond.global.rate=get-global-rate
context.get.old.all.global.rate=all-global-rate
context.get.old.all.pricing.rate=get-all-pricing-code


context.get.curve.data=orders/bond/getCurveData
context.get.recurring.data.with.product.id=recurring/by-product-id/
context.get.recurring.data.with.product.code=recurring/by-product-code/
context.get.bond.static.by.product.code=recurring/get-bond-static/
context.get.product.id.by.product.code=products/get-by-product-code/
context.get.holiday.with.start.date=recurring/get-holiday-with-start-date/


